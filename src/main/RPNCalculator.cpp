/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file RPNCalculator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Luke Johnson <edward212@live.missouristate.edu>
 *          Jacob Wakefield <wakefield515@live.missouristate.edu>
 *			Geoffrey Lewis <lew1996@live.missouristate.edu>
 * @brief   Definition of RPNCalculator.
 */

#include "RPNCalculator.h"
#include <string>
#include <iostream>
#include <sstream>

namespace csc232 {
    int RPNCalculator::evaluate(const std::string &postfixExpress) {
		int finalResult = 0;
		int result = 0;
        for(char ch : postfixExpress){
        	std::stringstream ss;
			std::string pushedElement;
			ss << ch;
			ss >> pushedElement;
			if(isOperand(ch)){
				stack.push(std::string(pushedElement));
			} else if(!isspace(ch)){
				int operand2 = std::stoi(stack.peek());
				stack.pop();
				int operand1 = std::stoi(stack.peek());
				stack.pop();
				switch(ch){
					case '+':result = operand2 + operand1; break;
					case '-':result = operand1 - operand2; break;
					case '*':result = operand2 * operand1; break;
					case '/':result = operand1 / operand2; break; 
				}	
				if (stack.isEmpty())
					finalResult = result;
				else
					stack.push(std::to_string(result));
			}	
		}
        return finalResult;
    } // end RPNCalculator::evaluate(const std::string&)

    bool RPNCalculator::isOperand(const char op) const {
        return isdigit(op);    
    } // end RPNCalculator::isOperand(const char) const
} // end namespace csc232