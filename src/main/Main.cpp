/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Luke Johnson <edward212@live.missouristate.edu>
 *          Jacob Wakefield <wakefield515@live.missouristate.edu>
 *			Geoffrey Lewis <lew1996@live.missouristate.edu>
 * @brief   Entry point for this application. This is nothing more than a playground.
 */

#include <cstdlib>
#include <iostream>
#include <memory>

#include "Stack.h"
#include "ArrayStack.h"
#include "RPNCalculator.h"

int main() {
    std::cout << "Welcome to Lab08" << std::endl;

    // A stack of strings
    auto stack = std::make_shared<csc232::ArrayStack<std::string>>();
    std::cout << "stack->isEmpty() = " << (stack->isEmpty() ? "true" : "false") << std::endl;
    bool itemPushed = stack->push("1");
    std::cout << "stack->pushed(\"1\") = " << (itemPushed ? "true" : "false") << std::endl;
    std::string topOfStack;
    try {
        topOfStack = stack->peek();
        std::cout << "stack->peek() = " << topOfStack << std::endl;
    } catch (csc232::PrecondViolatedExcept e) {
        std::cout << e.what() << std::endl;
    }

    bool topOfStackRemoved = stack->pop();
    std::cout << "stack->pop() = " << (topOfStackRemoved ? "true" : "false") << std::endl;

    try {
        topOfStack = stack->peek();
        std::cout << "stack->peek() = " << topOfStack << std::endl;
    } catch (csc232::PrecondViolatedExcept e) {
        std::cout << e.what() << std::endl;
    }

    csc232::RPNCalculator calculator;
    const std::string expression{"2 3 4 + *"};
    int result = calculator.evaluate(expression);
    std::cout << "2 3 4 + * = " << result << std::endl;

    return EXIT_SUCCESS;
} // end main()
